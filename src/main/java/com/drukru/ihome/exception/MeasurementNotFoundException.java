package com.drukru.ihome.exception;

public class MeasurementNotFoundException extends RuntimeException {
    public MeasurementNotFoundException(Long id) {
        super("Could not find measurement " + id);
    }
}

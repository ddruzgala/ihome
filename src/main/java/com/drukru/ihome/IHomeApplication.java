package com.drukru.ihome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(IHomeApplication.class, args);
	}

}

package com.drukru.ihome.service;

import com.drukru.ihome.exception.DeviceNotFoundException;
import com.drukru.ihome.model.Device;
import com.drukru.ihome.repository.DeviceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DeviceService {

    private final DeviceRepository deviceRepository;

    public List<Device> getAllDevices() {
        return deviceRepository.findAll();
    }

    public Device saveDevice(Device device) {
        return deviceRepository.save(device);
    }

    public List<Device> getDevicesByIds(List<Long> ids) {
        return deviceRepository.findAllById(ids);
    }

    public Device updateDevice(Device device, Long id) {
        return deviceRepository.findById(id)
                .map(d -> {
                    d.setName(device.getName());
                    return deviceRepository.save(d);
                })
                .orElseThrow(() -> new DeviceNotFoundException(id));
    }

    public void deleteDevice(Long id) {
        deviceRepository.deleteById(id);
    }
}

package com.drukru.ihome.service;

import com.drukru.ihome.exception.MeasurementNotFoundException;
import com.drukru.ihome.model.Measurement;
import com.drukru.ihome.repository.MeasurementRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MeasurementService {

    private final MeasurementRepository measurementRepository;

    public List<Measurement> getAllMeasurements() {
        return measurementRepository.findAll();
    }

    public List<Measurement> getMeasurementsByIds(List<Long> ids) {
        return measurementRepository.findAllById(ids);
    }

    public Measurement saveMeasurement(Measurement measurement) {
        return measurementRepository.save(measurement);
    }

    public void deleteMeasurement(Long id) {
        measurementRepository.deleteById(id);
    }

    public Measurement updateMeasurement(Measurement measurement, Long id) {
        return measurementRepository.findById(id)
                .map(m -> {
                    m.setValue(measurement.getValue());
                    return measurementRepository.save(m);
                })
                .orElseThrow(() -> new MeasurementNotFoundException(id));
    }
}

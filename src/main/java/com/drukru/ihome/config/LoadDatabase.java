package com.drukru.ihome.config;

import com.drukru.ihome.model.Device;
import com.drukru.ihome.model.Measurement;
import com.drukru.ihome.repository.DeviceRepository;
import com.drukru.ihome.repository.MeasurementRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(MeasurementRepository measurementRepository, DeviceRepository deviceRepository) {
        return args -> {
            log.info("Preloading " + measurementRepository.save(new Measurement(1L, 20.92)));
            log.info("Preloading " + measurementRepository.save(new Measurement(2L,19.07)));
            log.info("Preloading " + deviceRepository.save(new Device(1L,"Device 1")));
            log.info("Preloading " + deviceRepository.save(new Device(2L,"Device 2")));
        };
    }
}

package com.drukru.ihome.controller;

import com.drukru.ihome.model.Measurement;
import com.drukru.ihome.service.MeasurementService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
class MeasurementController {

    private final MeasurementService measurementService;

    @GetMapping("/measurements")
    public List<Measurement> all() {
        return measurementService.getAllMeasurements();
    }

    @PostMapping("/measurements")
    public Measurement newMeasurement(@RequestBody Measurement measurement) {
        return measurementService.saveMeasurement(measurement);
    }

    @GetMapping("/measurements/{ids}")
    public List<Measurement> getByIds(@PathVariable List<Long> ids) {
        return measurementService.getMeasurementsByIds(ids);
    }

    @PutMapping("/measurements/{id}")
    public Measurement replaceMeasurement(@RequestBody Measurement measurement, @PathVariable Long id) {
        return measurementService.updateMeasurement(measurement, id);
    }

    @DeleteMapping("/measurements/{id}")
    public void deleteMeasurement(@PathVariable Long id) {
        measurementService.deleteMeasurement(id);
    }
}

package com.drukru.ihome.controller;

import com.drukru.ihome.model.Device;
import com.drukru.ihome.service.DeviceService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;

    @GetMapping("/devices")
    public List<Device> all() {
        return deviceService.getAllDevices();
    }

    @PostMapping("/devices")
    public Device newDevice(@RequestBody Device device) {
        return deviceService.saveDevice(device);
    }

    @GetMapping("/devices/{ids}")
    public List<Device> getByIds(@PathVariable List<Long> ids) {
        return deviceService.getDevicesByIds(ids);
    }

    @PutMapping("/devices/{id}")
    public Device replaceDevice(@RequestBody Device device, @PathVariable Long id) {
        return deviceService.updateDevice(device, id);
    }

    @DeleteMapping("/devices/{id}")
    public void deleteDevice(@PathVariable Long id) {
        deviceService.deleteDevice(id);
    }
}
